menu "HOST"
config OS_USING_NIMBLE_HOST
	bool "Indicates that a BLE host is present."
	default y

	if OS_USING_NIMBLE_HOST
		config MYNEWT_VAL_BLE_HOST
			int 
			default 1

		config NIMBLE_CFG_HOST
			int 
			default 0 if !OS_USING_NIMBLE_HOST
			default 1 if OS_USING_NIMBLE_HOST


		config MYNEWT_VAL_BLE_HOST_THREAD_STACK_SIZE
			int "BLE host thread stack size"
			default 1024

		config MYNEWT_VAL_BLE_HOST_THREAD_PRIORITY
			int "BLE host thread priority"
			default 5


		config BLE_HS_AUTO_START
			bool "BLE host auto start"
			default y
			help
				Causes the BLE host to automatically start during system
				initialization.
		config MYNEWT_VAL_BLE_HS_AUTO_START
			int 
			default 0 if !BLE_HS_AUTO_START
			default 1 if BLE_HS_AUTO_START


		# Debug settings.
		menu "Debug settings"
			config BLE_HS_DEBUG
				bool "Extra runtime assertions enable"
				default n
			config MYNEWT_VAL_BLE_HS_DEBUG
				int 
				default 0 if !BLE_HS_DEBUG
				default 1 if BLE_HS_DEBUG


			config BLE_HS_PHONY_HCI_ACKS
				bool "Host simulating incoming acks enable"
				default n
				help
					Rather than wait for HCI acknowledgements from a controller, the
					host simulates incoming acks.  Only recommended for test code
					running in the simulator.
			config MYNEWT_VAL_BLE_HS_PHONY_HCI_ACKS
				int 
				default 0 if !BLE_HS_PHONY_HCI_ACKS
				default 1 if BLE_HS_PHONY_HCI_ACKS


			config BLE_HS_REQUIRE_OS
				bool "Specifies whether the host can depend on the kernel being present"
				default y
				help
					Specifies whether the host can depend on the kernel being present.
					This should only be disabled for unit tests running in the
					simulator.
			config MYNEWT_VAL_BLE_HS_REQUIRE_OS
				int 
				default 0 if !BLE_HS_REQUIRE_OS
				default 1 if BLE_HS_REQUIRE_OS
		endmenu


		# Monitor interface settings
		# menu "Monitor interface settings"
		# 	config BLE_MONITOR_UART
		# 		bool "Monitor interface over UART enable"
		# 		default n
		# 	config MYNEWT_VAL_BLE_MONITOR_UART
		# 		int 
		# 		default 0 if !BLE_MONITOR_UART
		# 		default 1 if BLE_MONITOR_UART


		# 	config MYNEWT_VAL_BLE_MONITOR_UART_DEV
		# 		string "Monitor interface UART device"
		# 		default "uart0"


		# 	config MYNEWT_VAL_BLE_MONITOR_UART_BAUDRATE
		# 		int "UART monitor interface baudrate"
		# 		default 1000000
		# 		help 
		# 			Baudrate for monitor interface UART.


		# 	config MYNEWT_VAL_BLE_MONITOR_UART_BUFFER_SIZE
		# 		int "UART monitor interface ringbuffer size"
		# 		default 64
		# 		help
		# 			Monitor interface ringbuffer size for UART.
		# 			This value should be a power of 2.


		# 	config BLE_MONITOR_RTT
		# 		bool "Monitor interface over RTT enable"
		# 		default n
		# 	config MYNEWT_VAL_BLE_MONITOR_RTT
		# 		int 
		# 		default 0 if !BLE_MONITOR_RTT
		# 		default 1 if BLE_MONITOR_RTT


		# 	config MYNEWT_VAL_BLE_MONITOR_RTT_BUFFER_NAME
		# 		string "RTT monitor interface upstream buffer name"
		# 		default "btmonitor"


		# 	config MYNEWT_VAL_BLE_MONITOR_RTT_BUFFER_SIZE
		# 		int "RTT monitor interface upstream buffer size"
		# 		default 256


		# 	config BLE_MONITOR_RTT_BUFFERED
		# 		bool "RTT monitor interface buffering enable"
		# 		default y
		# 		help
		# 			Enables buffering when using monitor interface over RTT. The data
		# 			are written to RTT once complete packet is created in intermediate
		# 			buffer. This allows to skip complete packet if there is not enough
		# 			space in RTT buffer (e.g. there is no reader connected). If disabled,
		# 			monitor will simply block waiting for RTT to free space in buffer.
		# 	config MYNEWT_VAL_BLE_MONITOR_RTT_BUFFERED
		# 		int 
		# 		default 0 if !BLE_MONITOR_RTT_BUFFERED
		# 		default 1 if BLE_MONITOR_RTT_BUFFERED


		# 	config MYNEWT_VAL_BLE_MONITOR_CONSOLE_BUFFER_SIZE
		# 		int "Console output's internal buffer Size"
		# 		default 128
		# 		help
		# 			Size of internal buffer for console output. Any line exceeding this
		# 			length value will be split.
		# endmenu


		# L2CAP settings.
		menu "L2CAP settings"
			config MYNEWT_VAL_BLE_L2CAP_MAX_CHANS
				int "L2CAP channels's number"
				default 3
				help 
					The number of L2CAP channels to allocate.  The default value allows
					for the signal, ATT, and SM channels for each connection.


			config MYNEWT_VAL_BLE_L2CAP_SIG_MAX_PROCS
				int "Concurrent L2CAP signal procedures's max number."
				default 1
				help
					The maximum number of concurrent L2CAP signal procedures.


			config BLE_L2CAP_JOIN_RX_FRAGS
				bool "Collapse incoming L2CAP fragments into a minimal set of mbufs"
				default y
				help
					Whether to collapse incoming L2CAP fragments into a minimal set of 
					mbufs. 
					y: Slower, more memory efficient. 
					n: Faster, less memory efficient.
			config MYNEWT_VAL_BLE_L2CAP_JOIN_RX_FRAGS
				int 
				default 0 if !BLE_L2CAP_JOIN_RX_FRAGS
				default 1 if BLE_L2CAP_JOIN_RX_FRAGS


			config MYNEWT_VAL_BLE_L2CAP_RX_FRAG_TIMEOUT
				int "Expiry time for incoming data packets (ms)"
				default 30000
				help 
					Expiry time for incoming data packets (ms).  If this much time
					passes since the previous fragment was received, the connection is
					terminated.  A value of 0 means no timeout.


			config MYNEWT_VAL_BLE_L2CAP_COC_MAX_NUM
				int "LE Connection Oriented Channels channels's max number"
				default 0
				help 
					Defines maximum number of LE Connection Oriented Channels channels.
					When set to (0), LE COC is not compiled in.


			config MYNEWT_VAL_BLE_L2CAP_COC_MPS
				int "MPS of L2CAP COC module. The default value was 'MYNEWT_VAL_MSYS_1_BLOCK_SIZE-8' before stack ported."
				# default MYNEWT_VAL_MSYS_1_BLOCK_SIZE-8
				default MYNEWT_VAL_MSYS_1_BLOCK_SIZE
				help 
					Defines the MPS of L2CAP COC module. This is actually NimBLE's internal
					L2CAP MTU. The default MPS size is chosen in a way, that the MPS plus
					the required HCI and L2CAP headers fit into the smallest available
					MSYS blocks.


			# config BLE_L2CAP_ENHANCED_COC
			# 	bool "LE Enhanced CoC mode enable"
			# 	default n
			# 	help
			# 		Enables LE Enhanced CoC mode. 
			# 		restrictions: (BLE_L2CAP_COC_MAX_NUM > 0) && (BLE_VERSION >= 52) if 1.
			# config MYNEWT_VAL_BLE_L2CAP_ENHANCED_COC
			# 	int 
			# 	default 0 if !BLE_L2CAP_ENHANCED_COC
			# 	default 1 if BLE_L2CAP_ENHANCED_COC
		endmenu


		# Security manager settings.
		menu "Security manager settings"
			config BLE_SM_LEGACY
				bool "Security manager legacy pairing"
				default y
			config MYNEWT_VAL_BLE_SM_LEGACY
				int 
				default 0 if !BLE_SM_LEGACY
				default 1 if BLE_SM_LEGACY


			config BLE_SM_SC
				bool "Security manager secure connections (4.2)."
				default y if BLE_MESH
				default n
			config MYNEWT_VAL_BLE_SM_SC
				int 
				default 0 if !BLE_SM_SC
				default 1 if BLE_SM_SC


			config MYNEWT_VAL_BLE_SM_MAX_PROCS
				int "Concurrent security manager procedures's max number"
				default 1
				help
					The maximum number of concurrent security manager procedures.


			# config MYNEWT_VAL_BLE_SM_IO_CAP
			# 	hex "IO capabilities to report during pairing"
			# 	default 0x03
			# 	help 
			# 		The IO capabilities to report during pairing.  Valid values are:
			# 			BLE_HS_IO_DISPLAY_ONLY
			# 			BLE_HS_IO_DISPLAY_YESNO
			# 			BLE_HS_IO_KEYBOARD_ONLY
			# 			BLE_HS_IO_NO_INPUT_OUTPUT
			# 			BLE_HS_IO_KEYBOARD_DISPLAY
			choice
				prompt "IO capabilities to report during pairing"
				default BLE_HS_IO_NO_INPUT_OUTPUT_BOOL
				config BLE_HS_IO_DISPLAY_ONLY_BOOL
					bool "0x00: BLE_HS_IO_DISPLAY_ONLY"
				config BLE_HS_IO_DISPLAY_YESNO_BOOL
					bool "0x01: BLE_HS_IO_DISPLAY_YESNO"
				config BLE_HS_IO_KEYBOARD_ONLY_BOOL
					bool "0x02: BLE_HS_IO_KEYBOARD_ONLY"
				config BLE_HS_IO_NO_INPUT_OUTPUT_BOOL
					bool "0x03: BLE_HS_IO_NO_INPUT_OUTPUT"
				config BLE_HS_IO_KEYBOARD_DISPLAY_BOOL
					bool "0x04: BLE_HS_IO_KEYBOARD_DISPLAY"
				help 
					The IO capabilities to report during pairing.  Valid values are:
						BLE_HS_IO_DISPLAY_ONLY
						BLE_HS_IO_DISPLAY_YESNO
						BLE_HS_IO_KEYBOARD_ONLY
						BLE_HS_IO_NO_INPUT_OUTPUT
						BLE_HS_IO_KEYBOARD_DISPLAY
			endchoice
		    config MYNEWT_VAL_BLE_SM_IO_CAP
				hex
				default 0x00 if BLE_HS_IO_DISPLAY_ONLY_BOOL
				default 0x01 if BLE_HS_IO_DISPLAY_YESNO_BOOL
				default 0x02 if BLE_HS_IO_KEYBOARD_ONLY_BOOL
				default 0x03 if BLE_HS_IO_NO_INPUT_OUTPUT_BOOL
				default 0x04 if BLE_HS_IO_KEYBOARD_DISPLAY_BOOL


			config BLE_SM_OOB_DATA_FLAG
				bool "Out-of-band pairing algorithm enable"
				default n
				help
					Whether the out-of-band pairing algorithm is advertised.
			config MYNEWT_VAL_BLE_SM_OOB_DATA_FLAG
				int 
				default 0 if !BLE_SM_OOB_DATA_FLAG
				default 1 if BLE_SM_OOB_DATA_FLAG


			config BLE_SM_BONDING
				bool "Bonding (persistence and restoration of secure links) enable"
				default n
			config MYNEWT_VAL_BLE_SM_BONDING
				int 
				default 0 if !BLE_SM_BONDING
				default 1 if BLE_SM_BONDING


			config BLE_SM_MITM
				bool "Man-in-the-middle protection enable"
				default n
				help 
					"Whether man-in-the-middle protection is advertised during pairing."
			config MYNEWT_VAL_BLE_SM_MITM
				int 
				default 0 if !BLE_SM_MITM
				default 1 if BLE_SM_MITM


			config BLE_SM_KEYPRESS
				bool "'Keypress support' advertise"
				default n
				help
					Whether keypress support is advertised during pairing.
			config MYNEWT_VAL_BLE_SM_KEYPRESS
				int 
				default 0 if !BLE_SM_KEYPRESS
				default 1 if BLE_SM_KEYPRESS


			# config MYNEWT_VAL_BLE_SM_OUR_KEY_DIST
			# 	int "Bitmap indicating which keys to distribute during pairing"
			# 	default 0
			# 	help 
			# 		A bitmap indicating which keys to distribute during pairing.  The
			# 		bits are defined as follows:
			# 			0x01: BLE_SM_PAIR_KEY_DIST_ENC
			# 			0x02: BLE_SM_PAIR_KEY_DIST_ID
			# 			0x04: BLE_SM_PAIR_KEY_DIST_SIGN
			# 			0x08: BLE_SM_PAIR_KEY_DIST_LINK
			choice
				prompt "Bitmap indicating which keys to distribute during pairing"
				default BLE_SM_PAIR_KEY_DIST_NONE_0_BOOL
				config BLE_SM_PAIR_KEY_DIST_NONE_0_BOOL
					bool "0x00: None"
				config BLE_SM_PAIR_KEY_DIST_ENC_0_BOOL
					bool "0x01: BLE_SM_PAIR_KEY_DIST_ENC"
				config BLE_SM_PAIR_KEY_DIST_ID_0_BOOL
					bool "0x02: BLE_SM_PAIR_KEY_DIST_ID"
				config BLE_SM_PAIR_KEY_DIST_SIGN_0_BOOL
					bool "0x04: BLE_SM_PAIR_KEY_DIST_SIGN"
				config BLE_SM_PAIR_KEY_DIST_LINK_0_BOOL
					bool "0x08: BLE_SM_PAIR_KEY_DIST_LINK"
				help 
					A bitmap indicating which keys to distribute during pairing.  The
					bits are defined as follows:
						0x01: BLE_SM_PAIR_KEY_DIST_ENC
						0x02: BLE_SM_PAIR_KEY_DIST_ID
						0x04: BLE_SM_PAIR_KEY_DIST_SIGN
						0x08: BLE_SM_PAIR_KEY_DIST_LINK
			endchoice
		    config MYNEWT_VAL_BLE_SM_OUR_KEY_DIST
				hex
				default 0x00 if BLE_SM_PAIR_KEY_DIST_NONE_0_BOOL
				default 0x01 if BLE_SM_PAIR_KEY_DIST_ENC_0_BOOL
				default 0x02 if BLE_SM_PAIR_KEY_DIST_ID_0_BOOL
				default 0x04 if BLE_SM_PAIR_KEY_DIST_SIGN_0_BOOL
				default 0x08 if BLE_SM_PAIR_KEY_DIST_LINK_0_BOOL

			# config MYNEWT_VAL_BLE_SM_THEIR_KEY_DIST
			# 	int "Bitmap indicating which keys to accept during pairing"
			# 	default 0
			# 	help 
			# 		A bitmap indicating which keys to accept during pairing.  The
			# 		bits are defined as follows:
			# 			0x01: BLE_SM_PAIR_KEY_DIST_ENC
			# 			0x02: BLE_SM_PAIR_KEY_DIST_ID
			# 			0x04: BLE_SM_PAIR_KEY_DIST_SIGN
			# 			0x08: BLE_SM_PAIR_KEY_DIST_LINK
			choice
				prompt "Bitmap indicating which keys to accept during pairing"
				default BLE_SM_PAIR_KEY_DIST_NONE_1_BOOL
				config BLE_SM_PAIR_KEY_DIST_NONE_1_BOOL
					bool "0x00: None"
				config BLE_SM_PAIR_KEY_DIST_ENC_1_BOOL
					bool "0x01: BLE_SM_PAIR_KEY_DIST_ENC"
				config BLE_SM_PAIR_KEY_DIST_ID_1_BOOL
					bool "0x02: BLE_SM_PAIR_KEY_DIST_ID"
				config BLE_SM_PAIR_KEY_DIST_SIGN_1_BOOL
					bool "0x04: BLE_SM_PAIR_KEY_DIST_SIGN"
				config BLE_SM_PAIR_KEY_DIST_LINK_1_BOOL
					bool "0x08: BLE_SM_PAIR_KEY_DIST_LINK"
				help 
					A bitmap indicating which keys to accept during pairing.  The
					bits are defined as follows:
						0x01: BLE_SM_PAIR_KEY_DIST_ENC
						0x02: BLE_SM_PAIR_KEY_DIST_ID
						0x04: BLE_SM_PAIR_KEY_DIST_SIGN
						0x08: BLE_SM_PAIR_KEY_DIST_LINK
			endchoice
		    config MYNEWT_VAL_BLE_SM_THEIR_KEY_DIST
				hex
				default 0x00 if BLE_SM_PAIR_KEY_DIST_NONE_1_BOOL
				default 0x01 if BLE_SM_PAIR_KEY_DIST_ENC_1_BOOL
				default 0x02 if BLE_SM_PAIR_KEY_DIST_ID_1_BOOL
				default 0x04 if BLE_SM_PAIR_KEY_DIST_SIGN_1_BOOL
				default 0x08 if BLE_SM_PAIR_KEY_DIST_LINK_1_BOOL


			config BLE_SM_SC_DEBUG_KEYS
				bool "SM debug mode enable"
				default n
				help
					Enable SM debug mode. In this mode SM uses predefined DH key pair as
					described in Core Specification 5.0, Vol. 3, Part H, 2.3.5.6.1. This
					allows to decrypt air traffic easily and thus should be only used
					for debugging.
			config MYNEWT_VAL_BLE_SM_SC_DEBUG_KEYS
				int 
				default 0 if !BLE_SM_SC_DEBUG_KEYS
				default 1 if BLE_SM_SC_DEBUG_KEYS
		endmenu


		# GAP options.
		config MYNEWT_VAL_BLE_GAP_MAX_PENDING_CONN_PARAM_UPDATE
			int "Number of connection parameter updates that can be pending simultaneously"
			default 1
			help 
				Controls the number of connection parameter updates that can be pending
				simultaneously. Devices with many concurrent connections may need
				to increase this value.


		# Supported GATT procedures.  By default:
		#	 o Notify and indicate are enabled;
		#	 o All other procedures are enabled for centrals.
		menu "Supported GATT procedures"
			config BLE_GATT_DISC_ALL_SVCS
				bool "Discover All Primary Services GATT procedure enable"
				default BLE_ROLE_CENTRAL
				help 
					Enables the Discover All Primary Services GATT procedure..
			config MYNEWT_VAL_BLE_GATT_DISC_ALL_SVCS
				int 
				default 0 if !BLE_GATT_DISC_ALL_SVCS
				default 1 if BLE_GATT_DISC_ALL_SVCS


			config BLE_GATT_DISC_SVC_UUID
				bool "Discover Primary Services by Service UUID GATT procedure enable"
				default BLE_ROLE_CENTRAL
				help 
					Enables the Discover Primary Services by Service UUID GATT procedure.
			config MYNEWT_VAL_BLE_GATT_DISC_SVC_UUID
				int 
				default 0 if !BLE_GATT_DISC_SVC_UUID
				default 1 if BLE_GATT_DISC_SVC_UUID


			config BLE_GATT_FIND_INC_SVCS
				bool "Find Included Services GATT procedure enable"
				default BLE_ROLE_CENTRAL
				help 
					Enables the Find Included Services GATT procedure.
			config MYNEWT_VAL_BLE_GATT_FIND_INC_SVCS
				int 
				default 0 if !BLE_GATT_FIND_INC_SVCS
				default 1 if BLE_GATT_FIND_INC_SVCS


			config BLE_GATT_DISC_ALL_CHRS
				bool "Discover All Characteristics of a Service GATT procedure enable"
				default BLE_ROLE_CENTRAL
				help 
					Enables the Discover All Characteristics of a Service GATT procedure.
			config MYNEWT_VAL_BLE_GATT_DISC_ALL_CHRS
				int 
				default 0 if !BLE_GATT_DISC_ALL_CHRS
				default 1 if BLE_GATT_DISC_ALL_CHRS


			config BLE_GATT_DISC_CHR_UUID
				bool "Discover Characteristics by UUID GATT procedure enable"
				default BLE_ROLE_CENTRAL
				help 
					Enables the Discover Characteristics by UUID GATT procedure.
			config MYNEWT_VAL_BLE_GATT_DISC_CHR_UUID
				int 
				default 0 if !BLE_GATT_DISC_CHR_UUID
				default 1 if BLE_GATT_DISC_CHR_UUID


			config BLE_GATT_DISC_ALL_DSCS
				bool "Discover All Characteristic Descriptors procedure enable"
				default BLE_ROLE_CENTRAL
				help 
					Enables the Discover All Characteristic Descriptors procedure.
			config MYNEWT_VAL_BLE_GATT_DISC_ALL_DSCS
				int 
				default 0 if !BLE_GATT_DISC_ALL_DSCS
				default 1 if BLE_GATT_DISC_ALL_DSCS


			config BLE_GATT_READ
				bool "Read Characteristic Value GATT procedure enable"
				default BLE_ROLE_CENTRAL
				help 
					Enables the Read Characteristic Value GATT procedure.
			config MYNEWT_VAL_BLE_GATT_READ
				int 
				default 0 if !BLE_GATT_READ
				default 1 if BLE_GATT_READ


			config BLE_GATT_READ_UUID
				bool "Read Using Characteristic UUID GATT procedure enable"
				default BLE_ROLE_CENTRAL
				help 
					Enables the Read Using Characteristic UUID GATT procedure.
			config MYNEWT_VAL_BLE_GATT_READ_UUID
				int 
				default 0 if !BLE_GATT_READ_UUID
				default 1 if BLE_GATT_READ_UUID


			config BLE_GATT_READ_LONG
				bool "Read Long Characteristic Values GATT procedure enable"
				default BLE_ROLE_CENTRAL
				help 
					Enables the Read Long Characteristic Values GATT procedure.
			config MYNEWT_VAL_BLE_GATT_READ_LONG
				int 
				default 0 if !BLE_GATT_READ_LONG
				default 1 if BLE_GATT_READ_LONG


			config BLE_GATT_READ_MULT
				bool "Read Multiple Characteristic Values GATT procedure enable"
				default BLE_ROLE_CENTRAL
				help 
					Enables the Read Multiple Characteristic Values GATT procedure.
			config MYNEWT_VAL_BLE_GATT_READ_MULT
				int 
				default 0 if !BLE_GATT_READ_MULT
				default 1 if BLE_GATT_READ_MULT


			config BLE_GATT_WRITE_NO_RSP
				bool "Write Without Response GATT procedure enable"
				default BLE_ROLE_CENTRAL
				help 
					Enables the Write Without Response GATT procedure.
			config MYNEWT_VAL_BLE_GATT_WRITE_NO_RSP
				int 
				default 0 if !BLE_GATT_WRITE_NO_RSP
				default 1 if BLE_GATT_WRITE_NO_RSP


			config BLE_GATT_SIGNED_WRITE
				bool "Signed Write Without Response GATT procedure enable"
				default BLE_ROLE_CENTRAL
				help 
					Enables the Signed Write Without Response GATT procedure.
			config MYNEWT_VAL_BLE_GATT_SIGNED_WRITE
				int 
				default 0 if !BLE_GATT_SIGNED_WRITE
				default 1 if BLE_GATT_SIGNED_WRITE


			config BLE_GATT_WRITE
				bool "Write Characteristic Value GATT procedure enable"
				default BLE_ROLE_CENTRAL
				help 
					Enables the Write Characteristic Value GATT procedure.
			config MYNEWT_VAL_BLE_GATT_WRITE
				int 
				default 0 if !BLE_GATT_WRITE
				default 1 if BLE_GATT_WRITE


			config BLE_GATT_WRITE_LONG
				bool "Write Long Characteristic Values GATT procedure enable"
				default BLE_ROLE_CENTRAL
				help 
					Enables the Write Long Characteristic Values GATT procedure.
			config MYNEWT_VAL_BLE_GATT_WRITE_LONG
				int 
				default 0 if !BLE_GATT_WRITE_LONG
				default 1 if BLE_GATT_WRITE_LONG


			config BLE_GATT_WRITE_RELIABLE
				bool "Reliable Writes GATT procedure enable"
				default BLE_ROLE_CENTRAL
				help 
					Enables the Reliable Writes GATT procedure.
			config MYNEWT_VAL_BLE_GATT_WRITE_RELIABLE
				int 
				default 0 if !BLE_GATT_WRITE_RELIABLE
				default 1 if BLE_GATT_WRITE_RELIABLE


			config BLE_GATT_NOTIFY
				bool "Sending and receiving of GATT notifications enable"
				default y
				help 
					Enables sending and receiving of GATT notifications.
			config MYNEWT_VAL_BLE_GATT_NOTIFY
				int 
				default 0 if !BLE_GATT_NOTIFY
				default 1 if BLE_GATT_NOTIFY


			config BLE_GATT_INDICATE
				bool "Sending and receiving of GATT indications enable"
				default y
				help 
					Enables sending and receiving of GATT indications.
			config MYNEWT_VAL_BLE_GATT_INDICATE
				int 
				default 0 if !BLE_GATT_INDICATE
				default 1 if BLE_GATT_INDICATE
		endmenu


		# GATT options.
		menu "GATT options"
			config MYNEWT_VAL_BLE_GATT_READ_MAX_ATTRS
				int "Max number of attributes that can be read with a single GATT Read Multiple Characteristic Values procedure"
				default 8
				help 
					The maximum number of attributes that can be read with a single
					GATT Read Multiple Characteristic Values procedure.

			config MYNEWT_VAL_BLE_GATT_WRITE_MAX_ATTRS
				int "Max number of attributes that can be written with a single GATT Reliable Write procedure"
				default 4
				help 
					The maximum number of attributes that can be written with a single
					GATT Reliable Write procedure.

			config MYNEWT_VAL_BLE_GATT_MAX_PROCS
				int "Max number of concurrent client GATT procedures."
				default 4


			config MYNEWT_VAL_BLE_GATT_RESUME_RATE
				int "Rate to periodically resume GATT procedures"
				default 1000
				help 
					The rate to periodically resume GATT procedures that have stalled
					due to memory exhaustion. (0/1)  Units are milliseconds.
		endmenu


		# Supported server ATT commands.
		menu "Supported server ATT commands"
			config BLE_ATT_SVR_FIND_INFO
				bool "Incoming Find Information Request ATT commands processing enable"
				default y
				help 
					Enables processing of incoming Find Information Request ATT commands.
			config MYNEWT_VAL_BLE_ATT_SVR_FIND_INFO
				int 
				default 0 if !BLE_ATT_SVR_FIND_INFO
				default 1 if BLE_ATT_SVR_FIND_INFO


			config BLE_ATT_SVR_FIND_TYPE
				bool "Incoming Find By Type Value Request ATT commands processing enable"
				default y
				help 
					Enables processing of incoming Find By Type Value Request ATT
					commands.
			config MYNEWT_VAL_BLE_ATT_SVR_FIND_TYPE
				int 
				default 0 if !BLE_ATT_SVR_FIND_TYPE
				default 1 if BLE_ATT_SVR_FIND_TYPE


			config BLE_ATT_SVR_READ_TYPE
				bool "Incoming Read By Type Request ATT commands processing enable"
				default y
				help 
					Enables processing of incoming Read By Type Request ATT commands.
			config MYNEWT_VAL_BLE_ATT_SVR_READ_TYPE
				int 
				default 0 if !BLE_ATT_SVR_READ_TYPE
				default 1 if BLE_ATT_SVR_READ_TYPE


			config BLE_ATT_SVR_READ
				bool "Incoming Read Request ATT commands processing enable"
				default y
				help 
					Enables processing of incoming Read Request ATT commands.
			config MYNEWT_VAL_BLE_ATT_SVR_READ
				int 
				default 0 if !BLE_ATT_SVR_READ
				default 1 if BLE_ATT_SVR_READ


			config BLE_ATT_SVR_READ_BLOB
				bool "Incoming Read Blob Request ATT commands processing enable"
				default y
				help 
					Enables processing of incoming Read Blob Request ATT commands.
			config MYNEWT_VAL_BLE_ATT_SVR_READ_BLOB
				int 
				default 0 if !BLE_ATT_SVR_READ_BLOB
				default 1 if BLE_ATT_SVR_READ_BLOB


			config BLE_ATT_SVR_READ_MULT
				bool "Incoming Read Multiple Request ATT commands processing enable"
				default y
				help 
					Enables processing of incoming Read Multiple Request ATT commands.
			config MYNEWT_VAL_BLE_ATT_SVR_READ_MULT
				int 
				default 0 if !BLE_ATT_SVR_READ_MULT
				default 1 if BLE_ATT_SVR_READ_MULT


			config BLE_ATT_SVR_READ_GROUP_TYPE
				bool "Incoming Read by Group Type Request ATT commands processing enable"
				default y
				help 
					Enables processing of incoming Read by Group Type Request ATT commands.
			config MYNEWT_VAL_BLE_ATT_SVR_READ_GROUP_TYPE
				int 
				default 0 if !BLE_ATT_SVR_READ_GROUP_TYPE
				default 1 if BLE_ATT_SVR_READ_GROUP_TYPE


			config BLE_ATT_SVR_WRITE
				bool "Incoming Write Request ATT commands processing enable"
				default y
				help 
					Enables processing of incoming Write Request ATT commands.
			config MYNEWT_VAL_BLE_ATT_SVR_WRITE
				int 
				default 0 if !BLE_ATT_SVR_WRITE
				default 1 if BLE_ATT_SVR_WRITE


			config BLE_ATT_SVR_WRITE_NO_RSP
				bool "Incoming Write Command ATT commands processing enable"
				default y
				help 
					Enables processing of incoming Write Command ATT commands.
			config MYNEWT_VAL_BLE_ATT_SVR_WRITE_NO_RSP
				int 
				default 0 if !BLE_ATT_SVR_WRITE_NO_RSP
				default 1 if BLE_ATT_SVR_WRITE_NO_RSP


			config BLE_ATT_SVR_SIGNED_WRITE
				bool "Incoming Signed Write Command ATT commands processing enable"
				default y
				help 
					Enables processing of incoming Signed Write Command ATT commands.
			config MYNEWT_VAL_BLE_ATT_SVR_SIGNED_WRITE
				int 
				default 0 if !BLE_ATT_SVR_SIGNED_WRITE
				default 1 if BLE_ATT_SVR_SIGNED_WRITE


			config BLE_ATT_SVR_QUEUED_WRITE
				bool "Incoming Prepare Write Request and Execute Write Request ATT commands processing enable"
				default y
				help
					Enables processing of incoming Prepare Write Request and Execute Write Request ATT commands.
			config MYNEWT_VAL_BLE_ATT_SVR_QUEUED_WRITE
				int 
				default 0 if !BLE_ATT_SVR_QUEUED_WRITE
				default 1 if BLE_ATT_SVR_QUEUED_WRITE


			config BLE_ATT_SVR_NOTIFY
				bool "Incoming Handle Value Notification ATT commands processing enable"
				default y
				help 
					Enables processing of incoming Handle Value Notification ATT commands.
			config MYNEWT_VAL_BLE_ATT_SVR_NOTIFY
				int 
				default 0 if !BLE_ATT_SVR_NOTIFY
				default 1 if BLE_ATT_SVR_NOTIFY


			config BLE_ATT_SVR_INDICATE
				bool "Incoming Handle Value Indication ATT commands processing enable"
				default y
				help 
					Enables processing of incoming Handle Value Indication ATT commands.
			config MYNEWT_VAL_BLE_ATT_SVR_INDICATE
				int 
				default 0 if !BLE_ATT_SVR_INDICATE
				default 1 if BLE_ATT_SVR_INDICATE
		endmenu


		# ATT options.
		menu "ATT options"
			config MYNEWT_VAL_BLE_ATT_PREFERRED_MTU
				int "Preferred MTU to indicate in MTU exchange commands."
				default 256
				help 
					The preferred MTU to indicate in MTU exchange commands.


			config MYNEWT_VAL_BLE_ATT_SVR_MAX_PREP_ENTRIES
				int "A GATT server uses these when a peer performs a 'write long characteristic values' or 'write long characteristic descriptors' procedure"
				default 64
				help 
					A GATT server uses these when a peer performs a "write long
					characteristic values" or "write long characteristic descriptors"
					procedure.  One of these resources is consumed each time a peer
					sends a partial write.


			config MYNEWT_VAL_BLE_ATT_SVR_QUEUED_WRITE_TMO
				int "Expiry time for incoming ATT queued writes (ms)"
				default 30000
				help 
					Expiry time for incoming ATT queued writes (ms).  If this much
					time passes since the previous prepared write was received, the
					connection is terminated.  A value of 0 means no timeout.
		endmenu


		# Privacy options.
		config MYNEWT_VAL_BLE_RPA_TIMEOUT
			int "Rate that new random addresses should be generated (seconds)."
			default 300
			help 
				The rate that new random addresses should be generated (seconds).


		# Store settings.
		menu "Store settings"
			config MYNEWT_VAL_BLE_STORE_MAX_BONDS
				int "Max number of bonds that can be persisted"
				default 3
				help 
					Maximum number of bonds that can be persisted.  Note: increasing
					this value may also require increasing the capacity of the
					underlying storage mechanism.


			config MYNEWT_VAL_BLE_STORE_MAX_CCCDS
				int "Max number of client characteristic configuration descriptors that can be persisted"
				default 8
				help 
					Maximum number of client characteristic configuration descriptors
					that can be persisted.  Note: increasing this value may also
					require increasing the capacity of the underlying storage
					mechanism.
		endmenu


		config BLE_MESH
			bool "Bluetooth Mesh support enable"
			default n
			help 
				This option enables Bluetooth Mesh support. The specific
				features that are available may depend on other features
				that have been enabled in the stack, such as GATT support.
		config MYNEWT_VAL_BLE_MESH
			int 
			default 0 if !BLE_MESH
			default 1 if BLE_MESH


		# Flow control settings.
		menu "Flow control settings"
			config BLE_HS_FLOW_CTRL
				bool "Host-side flow control enable"
				default n
				help
					Whether to enable host-side flow control.  This should only be
					enabled in host-only setups (i.e., not combined-host-controller).
			config MYNEWT_VAL_BLE_HS_FLOW_CTRL
				int 
				default 0 if !BLE_HS_FLOW_CTRL
				default 1 if BLE_HS_FLOW_CTRL


			config MYNEWT_VAL_BLE_HS_FLOW_CTRL_ITVL
				int "Host-side flow control inrterval"
				default 1000
				help 
					The interval, in milliseconds, that the host should provide
					number-of-completed-packets updates to the controller.


			config MYNEWT_VAL_BLE_HS_FLOW_CTRL_THRESH
				int "Host-side flow control threshold"
				default 2
				help 
					If the number of data buffers available to the controller falls to
					this number, immediately send a number-of-completed-packets event.
					The free buffer count is calculated as follows:
					(total-acl-bufs - bufs-freed-since-last-num-completed-event).


			config BLE_HS_FLOW_CTRL_TX_ON_DISCONNECT
				bool "Host side flow control cmd to on disconnect"
				default n
				help
					If enabled, the host will immediately transmit a
					host-number-of-completed-packets command whenever a connection
					terminates.  This behavior is not required by the standard, but is
					a necessary workaround when interfacing with some controllers.
			config MYNEWT_VAL_BLE_HS_FLOW_CTRL_TX_ON_DISCONNECT
				int 
				default 0 if !BLE_HS_FLOW_CTRL_TX_ON_DISCONNECT
				default 1 if BLE_HS_FLOW_CTRL_TX_ON_DISCONNECT
		endmenu

		config MYNEWT_VAL_BLE_HS_STOP_ON_SHUTDOWN
			int "Stops the Bluetooth host when the system shuts down"
			default 1
			help 
				Stops the Bluetooth host when the system shuts down.  Stopping
				entails aborting all GAP procedures and terminating open
				connections.


		config MYNEWT_VAL_BLE_HS_STOP_ON_SHUTDOWN_TIMEOUT
			int "Timeout used in NimBLE's host stop procedure in ms."
			default 2000


		config MYNEWT_VAL_BLE_HS_SYSINIT_STAGE
			int "Sysinit stage for the NimBLE host."
			default 200

		### Log settings.
		menu "Log settings"
			config MYNEWT_VAL_BLE_HS_LOG_MOD
				int "Numeric module ID to use for BLE host log messages."
				default 4


			config MYNEWT_VAL_BLE_HS_LOG_LVL
				int "Minimum level for the BLE host log."
				default 1
		endmenu

		menu "Services"
			source "$OS_ROOT/components/ble/mynewt-nimble/nimble/host/services/ans/Kconfig"
			source "$OS_ROOT/components/ble/mynewt-nimble/nimble/host/services/bas/Kconfig"
			source "$OS_ROOT/components/ble/mynewt-nimble/nimble/host/services/bleuart/Kconfig"
			source "$OS_ROOT/components/ble/mynewt-nimble/nimble/host/services/dis/Kconfig"
			source "$OS_ROOT/components/ble/mynewt-nimble/nimble/host/services/gap/Kconfig"
			source "$OS_ROOT/components/ble/mynewt-nimble/nimble/host/services/gatt/Kconfig"
			source "$OS_ROOT/components/ble/mynewt-nimble/nimble/host/services/ias/Kconfig"
			source "$OS_ROOT/components/ble/mynewt-nimble/nimble/host/services/ipss/Kconfig"
			source "$OS_ROOT/components/ble/mynewt-nimble/nimble/host/services/lls/Kconfig"
			source "$OS_ROOT/components/ble/mynewt-nimble/nimble/host/services/tps/Kconfig"
		endmenu
		menu "Store"
			source "$OS_ROOT/components/ble/mynewt-nimble/nimble/host/store/Kconfig"
		endmenu
		menu "Mesh"
			source "$OS_ROOT/components/ble/mynewt-nimble/nimble/host/mesh/Kconfig"
		endmenu

	endif
endmenu
