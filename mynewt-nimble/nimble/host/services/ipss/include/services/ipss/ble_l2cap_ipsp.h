/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

#ifndef H_BLE_LL_IPSP_
#define H_BLE_LL_IPSP_

//#include ""

#ifdef __cplusplus
extern "C" {
#endif

#define BLE_L2CAP_IPSP_CHAN_NUM_MAX MYNEWT_VAL(BLE_L2CAP_COC_MAX_NUM)

typedef enum
{
    L2CAP_INTF_UNKNOWN = 0,
    L2CAP_INTF_SERVER  = 1,
    L2CAP_INTF_CLIENT  = 2,
} l2cap_intf_type_t;

typedef void (*ble_l2cap_chan_linkup)(uint16_t conn_handle, l2cap_intf_type_t type);
typedef void (*ble_l2cap_chan_data_recv)(uint16_t conn_handle, const struct os_mbuf *data);

void ble_l2cap_ipsp_init(void);

int     ble_l2cap_ipsp_l2cap_connect(uint16_t                 conn_handle,
                                     ble_l2cap_chan_data_recv client_recv_func,
                                     ble_l2cap_chan_linkup    l2cap_linkup);
int     ble_ll_ipsp_l2cap_create_server(ble_l2cap_chan_data_recv server_recv_func, ble_l2cap_chan_linkup l2cap_linkup);
int     ble_l2cap_ipsp_send(uint16_t conn_handle, void *data, uint16_t len);
int     ble_l2cap_intf_send(uint8_t if_idx, struct os_mbuf *sdu_xmit);
uint8_t ble_l2cap_get_ifidx(uint8_t *dst);
void    ble_l2cap_set_ifidx(uint16_t conn_handle, uint8_t *dst);
#ifdef __cplusplus
}
#endif

#endif /* H_BLE_LL_IPSP_ */
