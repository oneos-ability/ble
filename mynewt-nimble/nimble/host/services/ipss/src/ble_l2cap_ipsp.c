/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
#include <stdint.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include <host/ble_gap.h>
#include "nimble-console/console.h"
#include "services/ipss/ble_l2cap_ipsp.h"
#include "ble_netif_lwip.h"
#include "../../../src/ble_l2cap_coc_priv.h"
#include <oneos_config.h>
#ifdef NET_USING_LWIP
#include <lwip/pbuf.h>
#include <lwip/netif.h>
#include <lwip/ip_addr.h>
#include <oneos_lwip.h>

#ifdef LWIP_USING_INTF_6LOWPAN_BLE
#include <netif/lowpan6_ble.h>
#endif /* end of LWIP_USING_INTF_6LOWPAN_BLE */

#if LWIP_IPV6
#include <lwip/ethip6.h>
#include <lwip/sockets.h>
#endif

#if LWIP_IPV6_DHCP6
#include <lwip/dhcp6.h>
#endif
#endif /* end of NET_USING_LWIP */

#define BLE_L2CAP_IPSP_DBG "ble_l2cap"
#include <dlog.h>

#define BLE_L2CAP_IPSP_PSM 0x0023

typedef struct ble_l2cap_ctrl
{
    l2cap_intf_type_t        type;
    uint16_t                 conn_handle;
    struct ble_l2cap_chan   *chan;
    ble_l2cap_chan_data_recv l2cap_client_recv_notice;
    ble_l2cap_chan_linkup    l2cap_client_linkup;
    uint8_t                  peeraddr[BLE_MAX_L2_ADDR_LEN];
} ble_l2cap_ipsp_ctrl_t;

typedef struct
{
    int                      conn_nums;
    ble_l2cap_chan_data_recv l2cap_server_recv_notice;
    ble_l2cap_chan_linkup    l2cap_server_linkup;
    ble_l2cap_ipsp_ctrl_t    ipsp_ctrl[BLE_L2CAP_IPSP_CHAN_NUM_MAX];
} ble_l2cap_ipsp_intf_t;

ble_l2cap_ipsp_intf_t gs_ipsp_intf;

void ble_l2cap_ipsp_init(void)
{
    ble_l2cap_ipsp_ctrl_t *l2cap_ipsp_ctrl;
    int                    index;

    gs_ipsp_intf.conn_nums                = 0;
    gs_ipsp_intf.l2cap_server_recv_notice = OS_NULL;
    for (index = 0; index < BLE_L2CAP_IPSP_CHAN_NUM_MAX; index++)
    {
        l2cap_ipsp_ctrl = &gs_ipsp_intf.ipsp_ctrl[index];

        l2cap_ipsp_ctrl->type                     = L2CAP_INTF_UNKNOWN;
        l2cap_ipsp_ctrl->conn_handle              = 0xffff;
        l2cap_ipsp_ctrl->chan                     = OS_NULL;
        l2cap_ipsp_ctrl->l2cap_client_recv_notice = OS_NULL;
        l2cap_ipsp_ctrl->l2cap_client_linkup      = OS_NULL;
        memset(&(l2cap_ipsp_ctrl->peeraddr), 0, 6);
    }

    return;
}

static l2cap_intf_type_t ble_l2cap_ipsp_get_intf_type(uint16_t conn_handle)
{
    ble_l2cap_ipsp_ctrl_t *l2cap_ipsp_ctrl;
    l2cap_intf_type_t      type;
    int                    index;

    type = L2CAP_INTF_UNKNOWN;
    for (index = 0; index < BLE_L2CAP_IPSP_CHAN_NUM_MAX; index++)
    {
        l2cap_ipsp_ctrl = &gs_ipsp_intf.ipsp_ctrl[index];

        if (l2cap_ipsp_ctrl->conn_handle == conn_handle)
        {
            type = l2cap_ipsp_ctrl->type;
            break;
        }
    }

    return type;
}

static struct ble_l2cap_chan *ble_l2cap_ipsp_get_intf_chan(uint16_t conn_handle)
{
    ble_l2cap_ipsp_ctrl_t *l2cap_ipsp_ctrl;
    struct ble_l2cap_chan *chan;
    int                    index;

    chan = OS_NULL;
    for (index = 0; index < BLE_L2CAP_IPSP_CHAN_NUM_MAX; index++)
    {
        l2cap_ipsp_ctrl = &gs_ipsp_intf.ipsp_ctrl[index];

        if (l2cap_ipsp_ctrl->conn_handle == conn_handle)
        {
            chan = l2cap_ipsp_ctrl->chan;
            break;
        }
    }

    return chan;
}

static void ble_l2cap_ipsp_update_intf(uint16_t conn_handle, struct ble_l2cap_chan *chan)
{
    ble_l2cap_ipsp_ctrl_t *l2cap_ipsp_ctrl;
    int                    index;

    for (index = 0; index < BLE_L2CAP_IPSP_CHAN_NUM_MAX; index++)
    {
        l2cap_ipsp_ctrl = &gs_ipsp_intf.ipsp_ctrl[index];

        if (l2cap_ipsp_ctrl->conn_handle == conn_handle)
        {
            l2cap_ipsp_ctrl->chan = chan;
            if (OS_NULL != l2cap_ipsp_ctrl->l2cap_client_linkup)
            {
                l2cap_ipsp_ctrl->l2cap_client_linkup(conn_handle, L2CAP_INTF_CLIENT);
            }
            break;
        }
    }

    return;
}

static int ble_l2cap_ipsp_add_server_intf(uint16_t conn_handle, struct ble_l2cap_chan *chan)
{
    ble_l2cap_ipsp_ctrl_t *l2cap_ipsp_ctrl;
    int                    index;
    int                    rc;

    rc = BLE_HS_ENOMEM;
    for (index = 0; index < BLE_L2CAP_IPSP_CHAN_NUM_MAX; index++)
    {
        l2cap_ipsp_ctrl = &gs_ipsp_intf.ipsp_ctrl[index];

        if (L2CAP_INTF_UNKNOWN == l2cap_ipsp_ctrl->type)
        {
            gs_ipsp_intf.conn_nums++;
            l2cap_ipsp_ctrl->type        = L2CAP_INTF_SERVER;
            l2cap_ipsp_ctrl->conn_handle = conn_handle;
            l2cap_ipsp_ctrl->chan        = chan;
            if (OS_NULL != gs_ipsp_intf.l2cap_server_linkup)
            {
                gs_ipsp_intf.l2cap_server_linkup(conn_handle, L2CAP_INTF_SERVER);
            }
            rc = 0;
            break;
        }
    }

    return rc;
}

static int ble_l2cap_ipsp_add_client_intf(uint16_t                 conn_handle,
                                          ble_l2cap_chan_data_recv client_recv_func,
                                          ble_l2cap_chan_linkup    l2cap_linkup)
{
    ble_l2cap_ipsp_ctrl_t *l2cap_ipsp_ctrl;
    int                    index;
    int                    rc;

    rc = BLE_HS_ENOMEM;
    for (index = 0; index < BLE_L2CAP_IPSP_CHAN_NUM_MAX; index++)
    {
        l2cap_ipsp_ctrl = &gs_ipsp_intf.ipsp_ctrl[index];

        if (L2CAP_INTF_UNKNOWN == l2cap_ipsp_ctrl->type)
        {
            gs_ipsp_intf.conn_nums++;
            l2cap_ipsp_ctrl->type                     = L2CAP_INTF_CLIENT;
            l2cap_ipsp_ctrl->conn_handle              = conn_handle;
            l2cap_ipsp_ctrl->l2cap_client_recv_notice = client_recv_func;
            l2cap_ipsp_ctrl->l2cap_client_linkup      = l2cap_linkup;
            rc                                        = 0;
            LOG_I(BLE_L2CAP_IPSP_DBG,
                  "add intf if_idx=%d, conn=%d, total=%d",
                  index + 1,
                  conn_handle,
                  gs_ipsp_intf.conn_nums);
            break;
        }
    }

    return rc;
}

static void ble_l2cap_ipsp_delete_intf(uint16_t conn_handle)
{
    ble_l2cap_ipsp_ctrl_t *l2cap_ipsp_ctrl;
    int                    index;

    for (index = 0; index < BLE_L2CAP_IPSP_CHAN_NUM_MAX; index++)
    {
        l2cap_ipsp_ctrl = &gs_ipsp_intf.ipsp_ctrl[index];

        if (l2cap_ipsp_ctrl->conn_handle == conn_handle)
        {
            gs_ipsp_intf.conn_nums--;
            l2cap_ipsp_ctrl->type        = L2CAP_INTF_UNKNOWN;
            l2cap_ipsp_ctrl->conn_handle = 0xffff;
            l2cap_ipsp_ctrl->chan        = OS_NULL;
            memset(&(l2cap_ipsp_ctrl->peeraddr), 0, 6);
            LOG_I(BLE_L2CAP_IPSP_DBG,
                  "delete intf if_idx=%d, conn=%d, total=%d",
                  index + 1,
                  conn_handle,
                  gs_ipsp_intf.conn_nums);
            break;
        }
    }

    return;
}

static void ble_l2cap_ipsp_recv_notice(uint16_t conn_handle, struct os_mbuf *rx_pdu)
{
    ble_l2cap_ipsp_ctrl_t *l2cap_ipsp_ctrl;
    int                    index;

    for (index = 0; index < BLE_L2CAP_IPSP_CHAN_NUM_MAX; index++)
    {
        l2cap_ipsp_ctrl = &gs_ipsp_intf.ipsp_ctrl[index];

        if (l2cap_ipsp_ctrl->conn_handle == conn_handle)
        {
            break;
        }
    }

    if (index < BLE_L2CAP_IPSP_CHAN_NUM_MAX)
    {
        if (L2CAP_INTF_SERVER == l2cap_ipsp_ctrl->type)
        {
            if (OS_NULL != gs_ipsp_intf.l2cap_server_recv_notice)
            {
                gs_ipsp_intf.l2cap_server_recv_notice(conn_handle, rx_pdu);
            }
            else
            {
                LOG_E(BLE_L2CAP_IPSP_DBG, "as l2cap server, not find recv notice func");
            }
        }
        else if (L2CAP_INTF_CLIENT == l2cap_ipsp_ctrl->type)
        {
            if (OS_NULL != l2cap_ipsp_ctrl->l2cap_client_recv_notice)
            {
                l2cap_ipsp_ctrl->l2cap_client_recv_notice(conn_handle, rx_pdu);
            }
            else
            {
                LOG_E(BLE_L2CAP_IPSP_DBG, "as l2cap client, not find recv notice func");
            }
        }
        else
        {
            LOG_E(BLE_L2CAP_IPSP_DBG, "why l2cap intf type is unknown, %04x", conn_handle);
        }
    }
    else
    {
        LOG_E(BLE_L2CAP_IPSP_DBG, "why not find l2cap intf, %04x", conn_handle);
    }

    return;
}

void ble_l2cap_ipsp_recv_prepare(uint16_t mtu, struct ble_l2cap_chan *chan)
{
    struct os_mbuf *sdu_rx;

    int rc;

    LOG_D(BLE_L2CAP_IPSP_DBG, "LE CoC accepting, chan: 0x%08lx, peer_mtu %d", (uint32_t)chan, mtu);

    sdu_rx = os_msys_get_pkthdr(mtu, 0);

    rc = ble_l2cap_recv_ready(chan, sdu_rx);
}

static void ble_ll_ipsp_l2cap_update_event(uint16_t conn_handle, int status, void *arg)
{
    struct ble_l2cap_chan *chan;

    chan = (struct ble_l2cap_chan *)arg;
    if (status == 0)
    {
        ble_l2cap_ipsp_add_server_intf(conn_handle, chan);
        LOG_D(BLE_L2CAP_IPSP_DBG, "L2CAP params updated");
    }
    else
    {
        LOG_E(BLE_L2CAP_IPSP_DBG, "L2CAP params update failed; rc=%d", status);
        ble_gap_terminate(conn_handle, 0x13);
    }
}

static char *ble_ll_addr_str(const void *addr)
{
    static char    buf[6 * 2 + 5 + 1];
    const uint8_t *u8p;

    u8p = addr;
    sprintf(buf, "%02x:%02x:%02x:%02x:%02x:%02x", u8p[5], u8p[4], u8p[3], u8p[2], u8p[1], u8p[0]);

    return buf;
}

static void ble_ll_print_conn_desc(const struct ble_gap_conn_desc *desc)
{
    LOG_I(BLE_L2CAP_IPSP_DBG,
          "handle=%d our_ota_addr_type=%d our_ota_addr=%s",
          desc->conn_handle,
          desc->our_ota_addr.type,
          ble_ll_addr_str(desc->our_ota_addr.val));
    LOG_I(BLE_L2CAP_IPSP_DBG,
          "our_id_addr_type=%d our_id_addr=%s",
          desc->our_id_addr.type,
          ble_ll_addr_str(desc->our_id_addr.val));
    LOG_I(BLE_L2CAP_IPSP_DBG,
          "peer_ota_addr_type=%d peer_ota_addr=%s",
          desc->peer_ota_addr.type,
          ble_ll_addr_str(desc->peer_ota_addr.val));
    LOG_I(BLE_L2CAP_IPSP_DBG,
          "peer_id_addr_type=%d peer_id_addr=%s",
          desc->peer_id_addr.type,
          ble_ll_addr_str(desc->peer_id_addr.val));
    LOG_I(BLE_L2CAP_IPSP_DBG,
          "conn_itvl=%d conn_latency=%d supervision_timeout=%d "
          "encrypted=%d authenticated=%d bonded=%d",
          desc->conn_itvl,
          desc->conn_latency,
          desc->supervision_timeout,
          desc->sec_state.encrypted,
          desc->sec_state.authenticated,
          desc->sec_state.bonded);
}

static int ble_ll_ipsp_l2cap_event(struct ble_l2cap_event *event, void *arg)
{
    int                        rc;
    struct os_mbuf            *data_buf;
    struct os_mbuf            *rx_pdu;
    static int                 data_len = 1000;
    static int                 send_cnt = 0;
    static bool                stalled  = false;
    struct ble_l2cap_chan_info chan_info;
    l2cap_intf_type_t          l2cap_type;
    struct ble_gap_conn_desc   desc;

    switch (event->type)
    {
    case BLE_L2CAP_EVENT_COC_CONNECTED:
        if (event->connect.status)
        {
            LOG_E(BLE_L2CAP_IPSP_DBG, "LE COC error: %d conn: %d", event->connect.status, event->connect.conn_handle);
            l2cap_type = ble_l2cap_ipsp_get_intf_type(event->connect.conn_handle);
            if (L2CAP_INTF_CLIENT == l2cap_type)
            {
                ble_l2cap_ipsp_delete_intf(event->connect.conn_handle);
            }
            return 0;
        }

        ble_gap_conn_find(event->connect.conn_handle, &desc);

        ble_l2cap_get_chan_info(event->connect.chan, &chan_info);

        LOG_D(BLE_L2CAP_IPSP_DBG,
              "LE COC connected, conn: %d, chan: 0x%08lx, scid: 0x%04x, "
              "dcid: 0x%04x, our_mtu: 0x%04x, peer_mtu: 0x%04x",
              event->connect.conn_handle,
              (uint32_t)event->connect.chan,
              chan_info.scid,
              chan_info.dcid,
              chan_info.our_l2cap_mtu,
              chan_info.peer_l2cap_mtu);

        struct ble_l2cap_sig_update_params params = {
            .itvl_min           = 0x0006, /* BLE_GAP_INITIAL_CONN_ITVL_MIN */
            .itvl_max           = 0x0006, /* BLE_GAP_INITIAL_CONN_ITVL_MIN */
            .slave_latency      = 0x0000,
            .timeout_multiplier = 0x0100,
        };

        l2cap_type = ble_l2cap_ipsp_get_intf_type(event->connect.conn_handle);
        if (L2CAP_INTF_UNKNOWN == l2cap_type)
        {
            LOG_I(BLE_L2CAP_IPSP_DBG, "l2cap server, recv connect, conn_handle=%04x", event->connect.conn_handle);
            rc = ble_l2cap_sig_update(event->connect.conn_handle,
                                      &params,
                                      ble_ll_ipsp_l2cap_update_event,
                                      event->connect.chan);
        }
        else if (L2CAP_INTF_CLIENT == l2cap_type)
        {
            LOG_I(BLE_L2CAP_IPSP_DBG,
                  "l2cap client, connect l2cap server succ, conn_handle=%04x",
                  event->connect.conn_handle);
            ble_l2cap_ipsp_update_intf(event->connect.conn_handle, event->connect.chan);
        }
        else
        {
            LOG_E(BLE_L2CAP_IPSP_DBG,
                  "why(l2cap server reconnect) happend this, conn_handle=%04x",
                  event->connect.conn_handle);
        }

        return 0;

    case BLE_L2CAP_EVENT_COC_DISCONNECTED:
        LOG_E(BLE_L2CAP_IPSP_DBG,
              "LE CoC disconnected, chan: 0x%08lx conn: %d",
              (uint32_t)event->disconnect.chan,
              event->disconnect.conn_handle);
        ble_l2cap_ipsp_delete_intf(event->disconnect.conn_handle);
        return 0;

    case BLE_L2CAP_EVENT_COC_ACCEPT:
        LOG_D(BLE_L2CAP_IPSP_DBG, "BLE_L2CAP_EVENT_COC_ACCEPT recv");
        ble_l2cap_ipsp_recv_prepare(event->accept.peer_sdu_size, event->accept.chan);
        return 0;

    case BLE_L2CAP_EVENT_COC_DATA_RECEIVED:
        LOG_D(BLE_L2CAP_IPSP_DBG, "BLE_L2CAP_EVENT_COC_DATA_RECEIVED recv");
        rx_pdu = event->receive.sdu_rx;

        struct os_mbuf_pkthdr *hdr;
        hdr = OS_MBUF_PKTHDR(rx_pdu);
        ble_l2cap_ipsp_recv_notice(event->receive.conn_handle, rx_pdu);

        os_mbuf_free_chain(rx_pdu);

        ble_l2cap_ipsp_recv_prepare(BLE_L2CAP_IPSP_MTU, event->receive.chan);
        break;

    case BLE_L2CAP_EVENT_COC_TX_UNSTALLED:
        LOG_D(BLE_L2CAP_IPSP_DBG, "L2CAP unstalled event");
        struct ble_l2cap_coc_endpoint *tx;
        struct ble_l2cap_chan         *chan;
        chan = event->tx_unstalled.chan;

        /*(void)chan->conn_handle;
          (void)event->tx_unstalled.chan->coc_tx;
           event->tx_unstalled.chan->coc_tx;
           tx = event->tx_unstalled.chan->coc_tx;
           TODO: exception deal */
        stalled = false;
        return 0;

    default:
        LOG_D(BLE_L2CAP_IPSP_DBG, "Other L2CAP event occurs: %d", event->type);
        return 0;
    }

    return 0;
}

int ble_ll_ipsp_l2cap_create_server(ble_l2cap_chan_data_recv server_recv_func, ble_l2cap_chan_linkup l2cap_linkup)
{
    int do_err;

    if (OS_NULL != gs_ipsp_intf.l2cap_server_recv_notice)
    {
        return BLE_HS_EAGAIN;
    }

    gs_ipsp_intf.l2cap_server_recv_notice = server_recv_func;
    gs_ipsp_intf.l2cap_server_linkup      = l2cap_linkup;
    do_err = ble_l2cap_coc_create_server(BLE_L2CAP_IPSP_PSM, BLE_L2CAP_IPSP_MTU, ble_ll_ipsp_l2cap_event, NULL);

    return do_err;
}

int ble_l2cap_ipsp_l2cap_connect(uint16_t                 conn_handle,
                                 ble_l2cap_chan_data_recv client_recv_func,
                                 ble_l2cap_chan_linkup    l2cap_linkup)
{
    struct os_mbuf *sdu_rx;
    int             rc;

    rc = ble_l2cap_ipsp_add_client_intf(conn_handle, client_recv_func, l2cap_linkup);

    sdu_rx = os_msys_get_pkthdr(BLE_L2CAP_IPSP_MTU, 0);

    LOG_D(BLE_L2CAP_IPSP_DBG, "begin l2cap connect, handle=0x%04x", conn_handle);
    rc = ble_l2cap_connect(conn_handle, BLE_L2CAP_IPSP_PSM, BLE_L2CAP_IPSP_MTU, sdu_rx, ble_ll_ipsp_l2cap_event, NULL);
    LOG_D(BLE_L2CAP_IPSP_DBG, "end l2cap connect, handle=0x%04x", conn_handle);
    return rc;
}

void ble_l2cap_set_ifidx(uint16_t conn_handle, uint8_t *dst)
{
    ble_l2cap_ipsp_ctrl_t *l2cap_ipsp_ctrl;
    int                    index;

    for (index = 0; index < BLE_L2CAP_IPSP_CHAN_NUM_MAX; index++)
    {
        l2cap_ipsp_ctrl = &gs_ipsp_intf.ipsp_ctrl[index];
        if (l2cap_ipsp_ctrl->conn_handle == conn_handle)
        {
            memcpy(&(l2cap_ipsp_ctrl->peeraddr), dst, 6);
            return;
        }
    }

    LOG_D(BLE_L2CAP_IPSP_DBG, "setifidx %d : %02x%02x%02x", conn_handle, dst[3], dst[4], dst[5]);
}

uint8_t ble_l2cap_get_ifidx(uint8_t *dst)
{
    ble_l2cap_ipsp_ctrl_t *l2cap_ipsp_ctrl;
    int                    index;
    uint8_t                broadcastmac[6] = {0xff, 0xff, 0xff, 0xff, 0xff, 0xff};

    if (!memcmp(&broadcastmac, dst, 6))
    {
        return 0xff; /* data send to uart and all ble */
    }

    if ((dst[0] & 0x01) == 0x01)
    {
        return 0xff; /* data send to uart and all ble */
    }

    for (index = 0; index < BLE_L2CAP_IPSP_CHAN_NUM_MAX; index++)
    {
        l2cap_ipsp_ctrl = &gs_ipsp_intf.ipsp_ctrl[index];
        if (!memcmp(&(l2cap_ipsp_ctrl->peeraddr), dst, 6))
        {
            LOG_D(BLE_L2CAP_IPSP_DBG, "find idx=%d", index + 1);
            return index + 1; /* data send to single ble */
        }
    }

    LOG_D(BLE_L2CAP_IPSP_DBG, "dst:%02x%02x%02x%02x%02x%02x", dst[0], dst[1], dst[2], dst[3], dst[4], dst[5]);
    return 0xfe; /* data send to uart */
}

int ble_l2cap_intf_send(uint8_t if_idx, struct os_mbuf *sdu_xmit)
{
    struct ble_l2cap_chan *l2cap_chan;
    l2cap_intf_type_t      type;
    int                    rc;
    uint8_t                src_addr[BLE_MAX_L2_ADDR_LEN];

    if (if_idx > BLE_L2CAP_IPSP_CHAN_NUM_MAX || !if_idx)
    {
        LOG_E(BLE_L2CAP_IPSP_DBG, "if_idx=%d is not exist", if_idx);
        return -1;
    }

    type = gs_ipsp_intf.ipsp_ctrl[if_idx - 1].type;
    if (type == L2CAP_INTF_UNKNOWN)
    {
        return -1;
    }

    l2cap_chan = gs_ipsp_intf.ipsp_ctrl[if_idx - 1].chan;
    if (OS_NULL == l2cap_chan)
    {
        LOG_E(BLE_L2CAP_IPSP_DBG,
              "why get conn_handle[%04x %d] info failed",
              gs_ipsp_intf.ipsp_ctrl[if_idx - 1].conn_handle,
              if_idx);
        return -1;
    }

    os_mbuf_copydata(sdu_xmit, 6, 6, src_addr);
    if (!memcmp(gs_ipsp_intf.ipsp_ctrl[if_idx - 1].peeraddr, src_addr, 6))
    {
        LOG_D(BLE_L2CAP_IPSP_DBG,
              "do not send pkt from myself %02x%02x%02x%02x%02x%02x",
              src_addr[0],
              src_addr[1],
              src_addr[2],
              src_addr[3],
              src_addr[4],
              src_addr[5]);
        return -1;
    }

    /* ble_l2cap_send takes ownership of the sdu */
    rc = ble_l2cap_send(l2cap_chan, sdu_xmit);

    return rc;
}

int ble_l2cap_ipsp_send(uint16_t conn_handle, void *data, uint16_t len)
{
    struct ble_l2cap_chan *l2cap_chan;
    struct os_mbuf        *sdu_xmit;
    int                    rc;

    l2cap_chan = ble_l2cap_ipsp_get_intf_chan(conn_handle);
    if (OS_NULL == l2cap_chan)
    {
        LOG_E(BLE_L2CAP_IPSP_DBG, "ipsp_send why get conn_handle[%04x] info failed", conn_handle);
        return -1;
    }

    sdu_xmit = os_msys_get_pkthdr(len, 0);

    LOG_I(BLE_L2CAP_IPSP_DBG, "Data buf %s", sdu_xmit ? "OK" : "NOK");

    os_mbuf_append(sdu_xmit, data, len);

    /* ble_l2cap_send takes ownership of the sdu */
    rc = ble_l2cap_send(l2cap_chan, sdu_xmit);
    LOG_I(BLE_L2CAP_IPSP_DBG, "data send, rc=%d", rc);

    if (rc != 0 && rc != BLE_HS_ESTALLED)
    {
        os_mbuf_free_chain(sdu_xmit);
    }

    return rc;
}

static os_err_t l2cap_send_func(int32_t argc, char **argv)
{
    int      buflen;
    char    *data;
    int      index;
    uint16_t conn_handle;
    int      rc;

    if (2 != argc && 3 != argc)
    {
        LOG_E(BLE_L2CAP_IPSP_DBG, "l2cap_send_func step 1, argc = %d", argc);
        return 0;
    }

    buflen = 256;
    if (3 == argc)
    {
        buflen = atoi(argv[2]);
    }

    conn_handle = atoi(argv[1]);

    data = malloc(buflen);
    if (OS_NULL == data)
    {
        LOG_E(BLE_L2CAP_IPSP_DBG, "l2cap_send_func step 2, alloc mem failed");
        return 0;
    }

    for (index = 0; index < buflen; index++)
    {
        data[index] = index & 0xFF;
    }

    rc = ble_l2cap_ipsp_send(conn_handle, data, buflen);
    LOG_I(BLE_L2CAP_IPSP_DBG, "data send stat, rc=%d, buflen = %d", rc, buflen);
    free(data);

    return 0;
}

extern struct os_mempool *os_msys_get_mempool(void);

static os_err_t mbuf_dump_func(int32_t argc, char **argv)
{
    struct os_mempool *mem;

    mem = os_msys_get_mempool();
    LOG_I(BLE_L2CAP_IPSP_DBG,
          "block_size=%d, num_blocks=%d, num_free=%d, min_free=%d",
          mem->mp_block_size,
          mem->mp_num_blocks,
          mem->mp_num_free,
          mem->mp_min_free);

    return 0;
}

static os_err_t l2cap_dump_func(int32_t argc, char **argv)
{
    ble_l2cap_ipsp_ctrl_t *l2cap_ipsp_ctrl;
    l2cap_intf_type_t      type;
    uint16_t               conn_handle;
    struct ble_l2cap_chan *chan;
    uint8_t                paddr[BLE_MAX_L2_ADDR_LEN];
    int                    index;

    LOG_I(BLE_L2CAP_IPSP_DBG, "active l2cap nums = %d", gs_ipsp_intf.conn_nums);

    for (index = 0; index < BLE_L2CAP_IPSP_CHAN_NUM_MAX; index++)
    {
        l2cap_ipsp_ctrl = &gs_ipsp_intf.ipsp_ctrl[index];
        type            = l2cap_ipsp_ctrl->type;
        conn_handle     = l2cap_ipsp_ctrl->conn_handle;
        chan            = l2cap_ipsp_ctrl->chan;
        memcpy(paddr, l2cap_ipsp_ctrl->peeraddr, 6);

        if (L2CAP_INTF_UNKNOWN != l2cap_ipsp_ctrl->type)
        {
            LOG_I(BLE_L2CAP_IPSP_DBG,
                  "[index=%d] conn=%d, chan=%08lx, type=%d, addr=%02x%02x%02x%02x%02x%02x",
                  index,
                  conn_handle,
                  chan,
                  type,
                  paddr[0],
                  paddr[1],
                  paddr[2],
                  paddr[3],
                  paddr[4],
                  paddr[5]);
        }
    }

    return 0;
}

#ifdef OS_USING_SHELL
#include <shell.h>
SH_CMD_EXPORT(mbuf_dump, mbuf_dump_func, "mbuf_dump");
SH_CMD_EXPORT(l2cap_dump, l2cap_dump_func, "l2cap dump");
SH_CMD_EXPORT(l2cap_send, l2cap_send_func, "l2cap send data [conn_handle] [data_len]");
#endif