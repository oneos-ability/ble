#include <inttypes.h>

#include "syscfg/syscfg.h"
#include "sysinit/sysinit.h"
#include "host/ble_hs.h"
#include "base64/base64.h"

#if MYNEWT_VAL(BLE_MESH_SETTINGS)
#include "mesh/glue.h"
#include "net.h"
#include "access.h"
#endif

#include "config/config.h"

#ifdef PKG_USING_EASYFLASH
#include "easyflash.h"
#else
#define ef_set_env_blob(...)
#define ef_get_env_blob(...) 0
#endif

#define BLE_STORE_CONFIG_SEC_ENCODE_SZ BASE64_ENCODE_SIZE(sizeof(struct ble_store_value_sec))

#define BLE_STORE_CONFIG_SEC_SET_ENCODE_SZ (MYNEWT_VAL(BLE_STORE_MAX_BONDS) * BLE_STORE_CONFIG_SEC_ENCODE_SZ + 1)

#if MYNEWT_VAL(BLE_STORE_CONFIG_PERSIST)
extern const struct conf_handler ble_store_config_conf_handler;
#endif

#if MYNEWT_VAL(BLE_MESH_SETTINGS)
struct mesh_setting
{
    const char *name;
    int (*func)(int argc, char **argv, char *val);
};
extern const struct mesh_setting settings[];
extern const struct conf_handler bt_mesh_settings_conf_handler;
#endif

int conf_register(const struct conf_handler *handler)
{
    return 0;
}

int conf_save_one(const char *name, char *value)
{
    os_kprintf("%s %s\r\n", name, value);
    if (NULL == value)
    {
        char value_null = '\0';
        ef_set_env_blob(name, &value_null, 1);
    }
    ef_set_env_blob(name, value, strlen(value));
    return 0;
}

#if MYNEWT_VAL(BLE_STORE_CONFIG_PERSIST)
char *(ble_conf_item[]) = {"our_sec", "peer_sec", "cccd"};

void ble_conf_load(void)
{
    char value[BLE_STORE_CONFIG_SEC_SET_ENCODE_SZ + 1];
    int len;
    char item_name[32];
    int i;
    for (i = 0; i < (sizeof(ble_conf_item) / sizeof(ble_conf_item[0])); i++)
    {
        sprintf(item_name, "ble_hs/%s", ble_conf_item[i]);
        len = ef_get_env_blob(item_name, value, sizeof(value), NULL);
        if (len)
        {
            value[len] = '\0';
            os_kprintf("%s %s\r\n", item_name, value);
            if ((1 == len) && ('\0' == value[0]))
            {
                ble_store_config_conf_handler.ch_set(1, &(ble_conf_item[i]), NULL);
            }
            else
            {
                ble_store_config_conf_handler.ch_set(1, &(ble_conf_item[i]), value);
            }
        }
    }
}
#endif

#if MYNEWT_VAL(BLE_MESH_SETTINGS)
extern int get_mesh_settings_array_size(void);
#if MYNEWT_VAL(BLE_MESH_SHELL)
extern const struct bt_mesh_comp *bt_mesh_comp_get_priv(void);
#else
// 'bt_mesh_comp_get_priv' should be realized by user if 'BLE_MESH_SHELL' is not enabled.
#ifndef bt_mesh_comp_get_priv
#define bt_mesh_comp_get_priv(...) 0
#endif
#endif

void mesh_conf_load(void)
{
    char value[128 + 1];
    int len;
    char item_name[32];
    int sta, i, j, k;
    int size = get_mesh_settings_array_size();
    char argv[3][12];
    char *argv_p[3];

    for (i = 0; i < size; i++)
    {
        if (!memcmp(settings[i].name, "AppKey", strlen("AppKey")))
        {
            sta = 0;
        }
        else if (!memcmp(settings[i].name, "NetKey", strlen("NetKey")))
        {
            sta = 1;
        }
        else if (!memcmp(settings[i].name, "RPL", strlen("RPL")))
        {
            sta = 2;
        }
#if MYNEWT_VAL(BLE_MESH_PROVISIONER)
        else if (!memcmp(settings[i].name, "Node", strlen("Node")))
        {
            sta = 3;
        }
#endif
#if CONFIG_BT_MESH_LABEL_COUNT > 0
        else if (!memcmp(settings[i].name, "Va", strlen("Va")))
        {
            sta = 4;
        }
#endif
        else if (!memcmp(settings[i].name, "v", strlen("v")))
        {
            sta = 5;
        }
        else if (!memcmp(settings[i].name, "s", strlen("s")))
        {
            sta = 6;
        }
        else
        {
            sta = 7;
        }

        if ((0 == sta) || (1 == sta) || (2 == sta) || (3 == sta) || (4 == sta))
        {
            int count;
            if (0 == sta)
            {
                count = MYNEWT_VAL(BLE_MESH_APP_KEY_COUNT);
            }
            else if (1 == sta)
            {
                count = MYNEWT_VAL(BLE_MESH_SUBNET_COUNT);
            }
            else if (2 == sta)
            {
                count = MYNEWT_VAL(BLE_MESH_CRPL);
            }
#if MYNEWT_VAL(BLE_MESH_PROVISIONER)
            else if (3 == sta)
            {
                count = CONFIG_BT_MESH_NODE_COUNT;
            }
#endif
#if CONFIG_BT_MESH_LABEL_COUNT > 0
            else if (4 == sta)
            {
                count = CONFIG_BT_MESH_LABEL_COUNT;
            }
#endif
            for (j = 0; j <= count; j++)
            {
                sprintf(item_name, "bt_mesh/%s/%x", settings[i].name, j);
                len = ef_get_env_blob(item_name, value, sizeof(value), NULL);
                if (len)
                {
                    value[len] = '\0';
                    os_kprintf("%s %s\r\n", item_name, value);
                    sprintf(argv[0], "%s", settings[i].name);
                    sprintf(argv[1], "%x", j);
                    argv_p[0] = argv[0];
                    argv_p[1] = argv[1];
                    if ((1 == len) && ('\0' == value[0]))
                    {
                        bt_mesh_settings_conf_handler.ch_set(2, argv_p, NULL);
                    }
                    else
                    {
                        bt_mesh_settings_conf_handler.ch_set(2, argv_p, value);
                    }
                }
            }
        }
        else if ((5 == sta) || (6 == sta))
        {
            const struct bt_mesh_comp *dev_comp = bt_mesh_comp_get_priv();
            int elem_count = dev_comp->elem_count;
            for (j = 0; j < elem_count; j++)
            {
                struct bt_mesh_elem *elem = &dev_comp->elem[j];
                int count;
                if (5 == sta)
                {
                    count = elem->vnd_model_count;
                }
                else if (6 == sta)
                {
                    count = elem->model_count;
                }
                for (k = 0; k < count; k++)
                {
                    char *ch_str[] = {"bind", "sub", "pub", "data"};
                    int x;
                    for (x = 0; x < 4; x++)
                    {
                        u16_t mod_key = (((u16_t)j << 8) | k);
                        sprintf(item_name, "bt_mesh/%s/%x/%s", settings[i].name, mod_key, ch_str[x]);
                        len = ef_get_env_blob(item_name, value, sizeof(value), NULL);
                        if (len)
                        {
                            value[len] = '\0';
                            os_kprintf("%s %s\r\n", item_name, value);
                            sprintf(argv[0], "%s", settings[i].name);
                            sprintf(argv[1], "%x", mod_key);
                            sprintf(argv[2], "%s", ch_str[x]);
                            argv_p[0] = argv[0];
                            argv_p[1] = argv[1];
                            argv_p[2] = argv[2];
                            if ((1 == len) && ('\0' == value[0]))
                            {
                                bt_mesh_settings_conf_handler.ch_set(3, argv_p, NULL);
                            }
                            else
                            {
                                bt_mesh_settings_conf_handler.ch_set(3, argv_p, value);
                            }
                        }
                    }
                }
            }
        }
        else
        {
            sprintf(item_name, "bt_mesh/%s", settings[i].name);
            len = ef_get_env_blob(item_name, value, sizeof(value), NULL);
            if (len)
            {
                value[len] = '\0';
                os_kprintf("%s %s\r\n", item_name, value);
                if ((1 == len) && ('\0' == value[0]))
                {
                    bt_mesh_settings_conf_handler.ch_set(1, (char **)(&(settings[i].name)), NULL);
                }
                else
                {
                    bt_mesh_settings_conf_handler.ch_set(1, (char **)(&(settings[i].name)), value);
                }
            }
        }
    }
    bt_mesh_settings_conf_handler.ch_commit();
    return;
}
#endif