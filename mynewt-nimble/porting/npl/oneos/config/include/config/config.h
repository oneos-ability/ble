#ifndef __CONFIG_H__
#define __CONFIG_H__

#include <os/queue.h>
#include <stdint.h>
#include <stdbool.h>

/**
 * Parameter to commit handler describing where data is going to.
 */
enum conf_export_tgt {
    /** Value is to be persisted */
    CONF_EXPORT_PERSIST,
    /** Value is to be display */
    CONF_EXPORT_SHOW
};

typedef enum conf_export_tgt conf_export_tgt_t;

/**
 * Handler for getting configuration items, this handler is called
 * per-configuration section.  Configuration sections are delimited
 * by '/', for example:
 *
 *  - section/name/value
 *
 * Would be passed as:
 *
 *  - argc = 3
 *  - argv[0] = section
 *  - argv[1] = name
 *  - argv[2] = value
 *
 * The handler returns the value into val, null terminated, up to
 * val_len_max.
 *
 * @param argc          The number of sections in the configuration variable
 * @param argv          The array of configuration sections
 * @param val           A pointer to the buffer to return the configuration
 *                      value into.
 * @param val_len_max   The maximum length of the val buffer to copy into.
 *
 * @return A pointer to val or NULL if error.
 */
typedef char *(*conf_get_handler_t)(int argc, char **argv, char *val, int val_len_max);
typedef char *(*conf_get_handler_ext_t)(int argc, char **argv, char *val, int val_len_max, void *arg);

/**
 * Set the configuration variable pointed to by argc and argv.  See
 * description of ch_get_handler_t for format of these variables.  This sets the
 * configuration variable to the shadow value, but does not apply the configuration
 * change.  In order to apply the change, call the ch_commit() handler.
 *
 * @param argc   The number of sections in the configuration variable.
 * @param argv   The array of configuration sections
 * @param val    The value to configure that variable to
 *
 * @return 0 on success, non-zero error code on failure.
 */
typedef int (*conf_set_handler_t)(int argc, char **argv, char *val);
typedef int (*conf_set_handler_ext_t)(int argc, char **argv, char *val, void *arg);

/**
 * Commit shadow configuration state to the active configuration.
 *
 * @return 0 on success, non-zero error code on failure.
 */
typedef int (*conf_commit_handler_t)(void);
typedef int (*conf_commit_handler_ext_t)(void *arg);

/**
 * Called per-configuration variable being exported.
 *
 * @param name The name of the variable to export
 * @param val  The value of the variable to export
 */
typedef void (*conf_export_func_t)(char *name, char *val);

/**
 * Export all of the configuration variables, calling the export_func
 * per variable being exported.
 *
 * @param export_func  The export function to call.
 * @param tgt          The target of the export, either for persistence or display.
 *
 * @return 0 on success, non-zero error code on failure.
 */
typedef int (*conf_export_handler_t)(conf_export_func_t export_func,
                                     conf_export_tgt_t tgt);
typedef int (*conf_export_handler_ext_t)(conf_export_func_t export_func,
                                         conf_export_tgt_t tgt, void *arg);

/**
 * Configuration handler, used to register a config item/subtree.
 */
struct conf_handler {
    SLIST_ENTRY(conf_handler) ch_list;
    /**
     * The name of the conifguration item/subtree
     */
    char *ch_name;

    /**
     * Whether to use the extended callbacks.
     * false: standard
     * true:  extended
     */
    bool ch_ext;

    /** Get configuration value */
    union {
        conf_get_handler_t ch_get;
        conf_get_handler_ext_t ch_get_ext;
    };

    /** Set configuration value */
    union {
        conf_set_handler_t ch_set;
        conf_set_handler_ext_t ch_set_ext;
    };

    /** Commit configuration value */
    union {
        conf_commit_handler_t ch_commit;
        conf_commit_handler_ext_t ch_commit_ext;
    };

    /** Export configuration value */
    union {
        conf_export_handler_t ch_export;
        conf_export_handler_ext_t ch_export_ext;
    };

    /** Custom argument that gets passed to the extended callbacks */
    void *ch_arg;
};

/**
 * Register a handler for configurations items.
 *
 * @param cf Structure containing registration info.
 *
 * @return 0 on success, non-zero on failure.
 */
int conf_register(const struct conf_handler *cf);

/**
 * Write a single configuration value to persisted storage (if it has
 * changed value).
 *
 * @param name Name/key of the configuration item.
 * @param var Value of the configuration item.
 *
 * @return 0 on success, non-zero on failure.
 */
int conf_save_one(const char *name, char *var);

void ble_conf_load(void);
void mesh_conf_load(void);

#endif