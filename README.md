mynewt-nimble:
    ver: v1.0.0
    intro: base on github apache/mynewt-nimble Commit:4d25a93af765c8d67c3806962571e1a39aed261e

See the official website for details about NimBLE：
https://mynewt.apache.org/latest/network/index.html

Nimble demos dir:
${OSROOT}/demos/components/ble/mynewt-nimble
